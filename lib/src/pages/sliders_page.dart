import 'package:flutter/material.dart';

class SlidersPage extends StatefulWidget {
  const SlidersPage({Key? key}) : super(key: key);

  @override
  State<SlidersPage> createState() => _SlidersPageState();
}

class _SlidersPageState extends State<SlidersPage> {
  double _slider = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Sliders"),),
      body: Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            _createSlider(),
            _createImage(),
          ],
        ),
      ),
    );
  }

  Widget _createSlider(){
    return Slider(
        min: 0,
        max: 100,
        value: _slider,
        divisions: 10,
        activeColor: Colors.redAccent,
        label: 'Valor actual ${_slider}',
        onChanged: (value) {
          setState(() {
            _slider = value;
          });
        }
    );
  }

  Widget _createImage(){
    return AnimatedContainer(
      curve: Curves.easeInOut,
      duration: Duration(milliseconds: 1000),
      child: Image(
        width: _slider*2,
        height: _slider*5,
        image: NetworkImage("https://1000marcas.net/wp-content/uploads/2019/11/Batman-Logo.png"),
      ),
    );
  }
}

