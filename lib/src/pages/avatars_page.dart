import 'package:flutter/material.dart';

class AvatarsPage extends StatelessWidget {
  const AvatarsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Avatars"),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 20),
            child: CircleAvatar(
              backgroundImage: NetworkImage("https://img.freepik.com/free-vector/cute-astronaut-dance-cartoon-vector-icon-illustration-technology-science-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-3851.jpg?w=2000"),
            )
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            child: CircleAvatar(
              child: Text("GL"),
              backgroundColor: Colors.brown,
            )
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage("https://cn.i.cdn.ti-platform.com/content/1555/el-mundo-de-craig/showpage/es/craig.48956a2a.jpg"),
          placeholder: AssetImage("assets/jar-loading.gif"),
        ),
      ),
    );
  }
}
