import 'package:flutter/material.dart';
import 'dart:math';

class AnimatedContainerPage extends StatefulWidget {
  const AnimatedContainerPage({Key? key}) : super(key: key);

  @override
  State<AnimatedContainerPage> createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.pink;
  IconData _icon_increase = Icons.arrow_forward_outlined;
  IconData _icon_decrease = Icons.arrow_back;
  BorderRadiusGeometry _border_radius = BorderRadius.circular(8);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Animaciones"),),
      body: Center(
        child: AnimatedContainer(
          curve: Curves.easeInExpo,
          duration: Duration(milliseconds: 1500, ),
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _border_radius,
            color: _color,
          ),
        ),
      ),
      floatingActionButton: Wrap(
        spacing: 5,
        crossAxisAlignment: WrapCrossAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: _decreaseWidth,
            child: Icon(_icon_decrease),
          ),
          FloatingActionButton(
            onPressed: _increaseWidth,
            child: Icon(_icon_increase),
          ),
          FloatingActionButton(
            onPressed: _nextShape,
            child: Icon(Icons.youtube_searched_for),
          ),
        ],
      )
    );
  }

  void _increaseWidth(){
    setState(() {
      _width+=5;
    });
  }

  void _decreaseWidth(){
    setState(() {
      _width-=5;
    });
  }

  _nextShape(){
    final rnd = Random();
    final int number = rnd.nextInt(_shapes.length - 1);
    Map<String,dynamic> shape = _shapes[number];
    setState(() {
      print(shape['height']);
      print('altura');
      //_height = double.parse('${shape['height']}.0');
      //_width = double.parse('${shape['width']}.0');

      _height = rnd.nextInt(500).toDouble();
      _width = rnd.nextInt(500).toDouble();
      _color = Color.fromRGBO(
          rnd.nextInt(255).toInt(),
          rnd.nextInt(255).toInt(),
          rnd.nextInt(255).toInt(), 1);
      _border_radius = BorderRadius.circular(rnd.nextInt(50).toDouble());
    });
  }

  final List<Map<String,dynamic>> _shapes = [
    {
      "width": 70.0,
      "height": 150.0,
      "color": Colors.amber,
      "border_radius": BorderRadius.circular(20.0)
    },
    {
      "width": 30.0,
      "height": 300.0,
      "color": Colors.blue,
      "border_radius": BorderRadius.circular(0)
    },
    {
      "width": 200.0,
      "height": 150.0,
      "color": Colors.redAccent,
      "border_radius": BorderRadius.circular(16.0)
    },
    {
      "width": 1000.0,
      "height": 70.0,
      "color": Colors.black,
      "border_radius": BorderRadius.circular(3.0)
    }
  ];
}
