import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:core';

class InputsPage extends StatefulWidget {
  const InputsPage({Key? key}) : super(key: key);

  @override
  State<InputsPage> createState() => _InputsPageState();
}

class _InputsPageState extends State<InputsPage> {
  String? _name;
  String? _email;
  String? _pass;
  String? _date;
  String? _selected_option;
  TextEditingController _inputFieldDateController = new TextEditingController();
  List<String> _powers = ["Fly", "Speedness", "Strongness"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Campos de texto"),),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        children: [
          _createSimpleInput(),
          _createEmailInput(),
          Divider(),
          _createPasswordInput(),
          Divider(),
          _createDatePicker(context),
          Divider(),
          _createDropdown(),
          _createTitle(),
        ],
      ),
    );
  }
  Widget _createSimpleInput (){
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        counter: Text('Cantidad ${ _name?.length ?? 0 }'),
        hintText: "Nombre de la persona",
        labelText: "Nombre",
        helperText: "descripcion",
        icon:Icon(Icons.account_box),
        suffixIcon: Icon(Icons.accessibility),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20)
        ),
      ),
      onChanged: (value){
        setState(() {
          _name = value;
        });
      },
    );
  }

  Widget _createTitle () {
    return ListTile(
      title: Text('Nombre es: ${_name ?? ''}'),
      trailing: Text(_selected_option.toString()),
    );
  }

  Widget _createEmailInput(){
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: "Escriba su correo",
        labelText: "Correo electronico",
        icon:Icon(Icons.email),
        suffixIcon: Icon(Icons.alternate_email),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
        ),
      ),
      onChanged: (value){
        setState(() {
          _email = value;
        });
      },
    );
  }

  Widget _createPasswordInput(){
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
        hintText: "Escriba su contraseña",
        labelText: "Contraseña",
        icon:Icon(Icons.lock),
        suffixIcon: Icon(Icons.password),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
      onChanged: (value){
        setState(() {
          _pass = value;
        });
      }
    );
  }

  Widget _createDatePicker(BuildContext context){
    return TextField(

      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20)
        ),
        hintText: "Seleccione una fecha",
        labelText: "Fecha de nacimiento",
        suffixIcon: Icon(Icons.date_range),
        icon: Icon(Icons.perm_contact_calendar),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate( context );
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime? datePicked = await showDatePicker(
      locale: Locale('es', 'ES'),
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2022),
      lastDate: new DateTime(2025),
    );
    if(datePicked != null){
      setState(() {
        _date = datePicked.toString();
        _inputFieldDateController.text = _date ?? '';
      });
    }
  }

  List<DropdownMenuItem<String>> _getOptionsDropDown(){
    return _powers.map<DropdownMenuItem<String>>((el) => DropdownMenuItem(child: Text(el), value: el,)).toList();
  }

  Widget _createDropdown(){

    return Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(width: 25,),
        Expanded(
          child: DropdownButton(
            borderRadius: BorderRadius.circular(30),
            icon: Icon(Icons.arrow_drop_down),
            value: _selected_option,
            items: _getOptionsDropDown(),
            onChanged: (e) {
              setState(() {
                _selected_option = e.toString();
              });
            }
          )
        )
      ]
    );
  }
}
