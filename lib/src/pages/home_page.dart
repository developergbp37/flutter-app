import 'package:flutter/material.dart';
import 'package:gabo_fullapp/src/providers/menu.provider.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final Map<String, IconData> _icons = {
    "add_alert": Icons.add_alert,
    "accessibility": Icons.accessibility,
    "folder_open": Icons.folder_open,
    "donut_large": Icons.donut_large,
    "close_fullscreen_outlined": Icons.close_fullscreen_outlined,
    "keyboard": Icons.keyboard,
    "list": Icons.list,
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
      ),
      body: FutureBuilder(
        future: menuProvider.loadData(),
        initialData: [],
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
          return _elementList(snapshot);
        },
      ),
    );
  }

  ListView _elementList(AsyncSnapshot snapshot) {
    return ListView.builder(
        itemCount: snapshot.data!.length,
        itemBuilder: (context, index) => ListTile(
              leading: Icon(
                _icons[snapshot.data![index]['icon'].toString()],
                color: Colors.blue[300],
              ),
              title: Text('${snapshot.data![index]['title']}'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                Navigator.pushNamed(context, snapshot.data![index]['route']);
              },
            ));
  }
}
