import 'package:flutter/material.dart';

class AlertsPage extends StatelessWidget {
  const AlertsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: RaisedButton(
            onPressed: () { _showAlert(context); },
            color: Colors.blueAccent,
            textColor: Colors.white,
            shape: StadiumBorder(),
            child: Text("mostrar texto"),
          ),
      ),
      appBar: AppBar(title: Text("Alertas"))
    );
  }

  void _showAlert(BuildContext context){
    showDialog(
      context: context, 
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          ),
          title: Text("Titulo"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Contenido de la caja"),
              FlutterLogo(size: 100,)
            ],
          ),
          actions: [
            FlatButton(
              onPressed: () => Navigator.of(context).pop(), 
              child: Text("cancelar")
            ),
            FlatButton(
              onPressed: () {}, 
              child: Text("ok"), color: Colors.blue[300], textColor: Colors.white,
            ),
          ],
        );
      } 
    );
  }
}