import 'package:flutter/material.dart';
import 'package:gabo_fullapp/src/pages/alerts_page.dart';
import 'package:gabo_fullapp/src/pages/animated_page.dart';
import 'package:gabo_fullapp/src/pages/avatars_page.dart';
import 'package:gabo_fullapp/src/pages/cards_page.dart';
import 'package:gabo_fullapp/src/pages/home_page.dart';
import 'package:gabo_fullapp/src/pages/inputs_page.dart';
import 'package:gabo_fullapp/src/pages/sliders_page.dart';

Map <String, WidgetBuilder> Routes(){
  return <String, WidgetBuilder>{
    "/": (BuildContext context) => HomePage(),
    "alert": (BuildContext context) => AlertsPage(),
    "avatar": (BuildContext context) => AvatarsPage(),
    "card": (BuildContext context) => CardsPage(),
    "animation": (BuildContext context) => AnimatedContainerPage(),
    "input": (BuildContext context) => const InputsPage(),
    "slider": (BuildContext context) => const SlidersPage(),
  };
}